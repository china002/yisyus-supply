<?php

namespace yisyus\supply\utils;

class Enum
{
    const SUPPLY_URL = 'http://scm.yisyu.com';
    const URI_PREFIX = '/supply';
    const PRODUCT_CATEGORY_URI = '/product/category/list';

    const PRODUCT_LIST_URI = '/product/list';
    const PRODUCT_DETAIL_URI = '/product/detail';
    const PRODUCT_BATCH_DETAIL_URI = '/product/batchDetail';
    const ORDER_FREIGHT_URI = '/order/freight';
    const ORDER_PUSH_URI = '/order/push';
    const ORDER_LIST_URI = '/order/list';
    const ORDER_DETAIL_URI = '/order/detail';

}