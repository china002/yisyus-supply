<?php

namespace yisyus\supply\utils;

class Sign
{
    /**
     * 签名
     * @param $data
     * @param $appsecret
     * @return string
     */
    public static function setSign($data, $appsecret ,$body = '')
    {
        ksort($data);
        $body = str_replace(" ","",$body);

        $str_key="";
        foreach ($data as $k=>$v){
            $str_key.=$k.$v;
        }
        $str_key .= $appsecret;
        $str_key .= $body;
        $sign = strtoupper(md5(sha1($str_key)));

        return $sign;
    }
}