<?php

namespace yisyus\supply\api;

use yisyus\supply\utils\Enum;

class SupplyApi extends BaseApi
{
    /**
     * 产品分类列表
     * @param $pid int 上级分类
     * @param $page int 页数
     * @param $size int 每页获取数量
     * @return mixed
     */
    public function productCategory($pid = 0, $page = 0, $size = 0, callable $back = null)
    {
        $params = [];
        if ($pid > 0) $params['pid'] = $pid;
        if ($page > 0) $params['page'] = $page;
        if ($size > 0) $params['size'] = $size;
        return $this->httpPost(Enum::PRODUCT_CATEGORY_URI, $params, $back);
    }

    /**
     * @param $page int 页数
     * @param $size int 每页获取数量
     * @param $cate_id int 分类ID
     * @param $search string 搜索内容
     * @param $min_price float 产品起始价
     * @param $max_price float 产品截止价
     * @return mixed
     */
    public function productList($page, $size, $cate_id = 0, $search = '', $min_price = 0, $max_price = 0, callable $back = null)
    {
        $params = [
            'page' => $page,
            'size' => $size,
        ];
        if ($cate_id > 0) $params['cate_id'] = $cate_id;
        if ($min_price > 0) $params['min_price'] = $min_price;
        if ($max_price > 0) $params['max_price'] = $max_price;
        if (!empty($search)) $params['search'] = $search;
        return $this->httpPost(Enum::PRODUCT_LIST_URI, $params, $back);
    }

    /**
     * 产品详情
     * @param $product_id
     * @return mixed
     */
    public function productDetail($product_id, callable $back = null)
    {
        $params = ['product_id' => $product_id,];
        return $this->httpPost(Enum::PRODUCT_DETAIL_URI, $params, $back);
    }

    /**
     * 批量产品详情
     * @param $product_ids string 产品ID，多个以英文逗号分隔
     * @return mixed
     */
    public function batchProductDetail($product_ids,$page,$size, callable $back = null)
    {
        $params = [
            'product_ids' => $product_ids,
            'page' => $page,
            'size' => $size,
        ];
        return $this->httpPost(Enum::PRODUCT_BATCH_DETAIL_URI, $params, $back);
    }

    /**
     * 获取产品邮费
     * @param $product_id int 产品ID
     * @param $product_attr_unique string 产品属性唯一值
     * @param $cart_num int 购买数量
     * @param $city string 收货所在城市名称
     * @return mixed
     */
    public function freight($product_id, $product_attr_unique, $cart_num, $city, callable $back = null)
    {
        $params = [
            'product_id' => $product_id,
            'product_attr_unique' => $product_attr_unique,
            'cart_num' => $cart_num,
            'city' => $city,
        ];
        return $this->httpPost(Enum::ORDER_FREIGHT_URI, $params, $back);
    }

    /**
     * @param $product_id int 产品ID
     * @param $product_attr_unique string 产品属性唯一值
     * @param $cart_num int 购买数量
     * @param $real_name string 收件人
     * @param $phone int 收件人手机号
     * @param $province string 收件省
     * @param $city string 收件市
     * @param $district string 收件区县街道
     * @param $detail string 收件详细地址
     * @param $mark string
     * @return mixed
     */
    public function pushOrder($product_id, $product_attr_unique, $cart_num, $real_name, $phone, $province, $city, $district, $detail, $mark = '', callable $back = null)
    {
        $params = [
            'product_id' => $product_id,
            'product_attr_unique' => $product_attr_unique,
            'cart_num' => $cart_num,
            'real_name' => $real_name,
            'phone' => $phone,
            'province' => $province,
            'city' => $city,
            'district' => $district,
            'detail' => $detail,
        ];
        if (!empty($mark)) {
            $params['mark'] = $mark;
        }
        return $this->httpPost(Enum::ORDER_PUSH_URI, $params, $back);
    }

    /**
     * 订单列表
     * @param $page int 页数
     * @param $size int 每页获取的条数
     * @param $status int 订单状态 0待发货、1待收货、2已完成
     * @return mixed
     */
    public function orderList($page, $size, $status, callable $back = null)
    {
        $params = [
            'page' => $page,
            'size' => $size,
            'status' => $status,
        ];
        return $this->httpPost(Enum::ORDER_LIST_URI, $params, $back);
    }

    /**
     * 订单详情
     * @param $order_id int 订单ID
     * @return mixed
     */
    public function orderDetail($order_id, callable $back = null)
    {
        return $this->httpPost(Enum::ORDER_DETAIL_URI, ['order_id' => $order_id], $back);
    }
}